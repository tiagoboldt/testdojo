Feature: Add a Task via API

	Scenario Outline: Adding a task 
		Given a Task with description <d>
		And user with name <n>
		When I make a POST with task to /api/tasks
		Then the Task is saved

	Examples: 
	| d | n |
	| Comprar pão | João |
	| Comprar Leite | Maria |