Feature: Delete Tasks via API

	Scenario: List tasks
		Given the url /api/tasks/:id
		When I make a DELETE request
		Then I get a JSON of all existing tasks