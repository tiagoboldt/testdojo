Feature: Update a Task via API

	Scenario Outline: Updating a task 
		Given a random task with ID <id>
		And a Task with description <d>
		And user with name <n>
		When I make a POST with the task to /api/tasks/<id>
		Then the Task previous task is substituted 
		

	Examples: 
	| id | d | n |
	| 1 | Comprar pão | João |
	| 2 | Comprar Leite | Maria |