from django.db import models

# Create your models here.


class Task(models.Model):
    created = models.DateTimeField('date created')
    owner = models.CharField(max_length=200)
    description = models.TextField()

