# -*- coding: utf-8 -*- 
from django.test import TestCase
from tasks.models import Task


class TaskTest(TestCase):
    def test_minimization(self):
    	"""minimize caps description to first 10 chars"""
    	description = 'Tenho que ir comprar pão'
        task = Task(description = description)
        self.assertEqual(description[:10], task.minimize())
